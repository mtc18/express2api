const express = require('express')
const router = express.Router()
// creamos el enrutador y a continuación le damos una ruta

const productoController = require('../../controllers/v2/productoController')
// las rutas que pongamos nos llevarán a los métodos del controlador

// middleware

//router.use(auth) // para todas las rutas que se vienen, o para alguna en particular:
const auth=require('../../middlewares/auth')


router.get('/', auth, productoController.index)
router.post('/', auth, productoController.create)
// solo para el método index de este controlador, aplicando el método auth

router.get('/', (req, res) => {
  // index
  productoController.index(req, res)
})

router.post('/', (req, res) => {
  productoController.create(req, res)
})

router.get('/:id', (req, res) => {
  // show con el id
  productoController.show(req, res)
})


router.delete('/:id', (req, res) => {
  productoController.remove(req, res)
})

router.put('/:id', (req, res) => {
  productoController.update(req, res)
})

router.get('/search', (req, res) => {
  // search con el nombre
  productoController.search(req, res)
})

module.exports = router
