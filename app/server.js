const express = require('express') // llamamos a Express
const app = express()
// estas dos líneas siempre. Es para ejecutar express.
const bodyParser = require('body-parser')
require('./config/db.js')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

//const router = require('./routes.js') // para coger nuestro módulo
const router2 = require('./routes2.js') // para coger nuestro módulo

const port = process.env.PORT || 8080 // establecemos nuestro puerto

//app.use('/api', router) // todo lo que empiece por /api va a "router"
app.use('/api2', router2) // todo lo que empiece por /api va a "router"

// iniciamos nuestro servidor
app.listen(port, () => {
  console.log('API escuchando en el puerto ' + port)
})
