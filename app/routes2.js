const express = require('express')

const router = express.Router()
const routerCervezas = require('./routes/v2/cervezas.js') // creamos una variable que es de esta ruta.
const routerProductos = require('./routes/v2/productos.js')
const routerUsers = require('./routes/v2/users.js')

// const Cerveza = require('./models/v2/Cerveza.js') --> esto va en el controlador

/* Esto va en -->  routes/v2/cervezas.js tiene la ruta que va al controlador.
router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra Api con MongoDB!' })
})
*/

// ruta d prueba mongoose. Con ésto creamos directos en BBDD. No nos redirige a una vista.
/* router.get('/ambar', (req, res) => {
  const miCerveza = new Cerveza({ nombre: 'Ambar' })
  miCerveza.save((err, miCerveza) => {
    if (err) return console.error(err)
    console.log(`Guardada en bbdd ${miCerveza.nombre}`)
  })
}) */

router.use('/cervezas', routerCervezas) // esta es la ruta asignada para acceder ahí (line 4)
router.use('/productos', routerProductos)
router.use('/users', routerUsers)
module.exports = router
